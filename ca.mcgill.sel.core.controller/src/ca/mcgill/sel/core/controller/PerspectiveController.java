package ca.mcgill.sel.core.controller;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;

/**
 * The controller for {@link COREModelElementMapping}.
 * 
 * @author Bowen
 *
 */
public class PerspectiveController extends CoreBaseController {
    
    /**
     * Add a mapping to the scene with an EMF command.
     *
     * @param firstModelElement first model element to be mapped
     * @param secondModelElement second model element to be mapped
     * @param nextLEMid id of the mapping
     * @param scene the given {@link COREScene}
     */
    public void createMapping(EObject firstModelElement, EObject secondModelElement, int nextLEMid, COREScene scene) {
        // create mapping
        COREModelElementMapping mapping = CoreFactory.eINSTANCE.createCOREModelElementMapping();
        mapping.getModelElements().add(firstModelElement);
        mapping.getModelElements().add(secondModelElement);

        //set nextLEMid on the mapping
        mapping.setLEMid(nextLEMid);

        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(scene);
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(AddCommand.create(editingDomain, scene,
                CorePackage.Literals.CORE_MODEL_ELEMENT_MAPPING, mapping));
        doExecute(editingDomain, compoundCommand);
    }
    
    /**
     * Remove a mapping from the scene with an EMF command.
     * 
     * @param mapping the given {@link COREModelElementMapping}
     * @param scene the given {@link COREScene}
     */
    public void removeMapping(COREModelElementMapping mapping, COREScene scene) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(scene);
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(RemoveCommand.create(editingDomain, scene, 
                CorePackage.Literals.CORE_MODEL_ELEMENT_MAPPING, mapping));

        doExecute(editingDomain, compoundCommand);
    }
}

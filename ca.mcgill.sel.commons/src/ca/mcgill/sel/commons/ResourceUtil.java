package ca.mcgill.sel.commons;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.eclipse.emf.common.util.URI;

/**
 * A utility class that supports both to locate resources in the file system and
 * JAR files.
 *
 * @author mschoettle
 */
public final class ResourceUtil {

    /**
     * Creates a new instance.
     */
    private ResourceUtil() {

    }

    /**
     * Returns the location of where the given class is executed in. In the IDE,
     * this usually is the bin directory, whereas in a JAR file this is the JAR
     * file.
     *
     * @param clazz the class of interest
     * @return the code execution location
     */
    public static URL getCodeLocation(Class<?> clazz) {
        return clazz.getProtectionDomain().getCodeSource().getLocation();
    }

    /**
     * Returns the absolute path to the directory of where the given class is
     * executed in. In the IDE, this is usually the parent of the bin directory,
     * whereas for a JAR file this is the containing directory of the JAR.
     *
     * @param clazz the class of interest
     * @return the absolute path of the main directory
     * @see #getCodeLocation(Class)
     */
    public static String getMainDirectory(Class<?> clazz) {
        URL codeLocation = getCodeLocation(clazz);
        String mainDirectory = null;

        try {
            // Easier to work with URIs. If the protocol is important, change to
            // codeLocation.toExternalForm().
            URI codeLocationURI = URI.createURI(URLDecoder.decode(codeLocation.getPath(), "UTF-8"));

            if (codeLocationURI.lastSegment().endsWith("jar")) {
                mainDirectory = codeLocationURI.trimSegments(1).toString();
            } else {
                mainDirectory = codeLocationURI.trimSegments(2).toString();
            }

            if (mainDirectory != null) {
                mainDirectory += "/";
            }
        } catch (UnsupportedEncodingException e) {
            // Ignore.
            e.printStackTrace();
        }

        return mainDirectory;
    }

    /**
     * Returns the absolute path to the resources directory. In the case of running
     * a Mac App Bundle, the resources directory is the one within the .app, e.g.,
     * MyApp.app/Contents/Resources/.
     *
     * @param clazz the class of interest
     * @return the absolute path of the resources directory
     * @see #getMainDirectory(Class)
     */
    public static String getResourcesDirectory(Class<?> clazz) {
        String resourcesDirectory = getMainDirectory(clazz);
        if (resourcesDirectory.contains(".app/Contents/Java/")) {
            resourcesDirectory = resourcesDirectory.replace(".app/Contents/Java/", ".app/Contents/Resources/");
        }
        return resourcesDirectory;
    }

    /**
     * Returns the {@link URL} of the given resource. The resource needs to be
     * located inside a source folder and the resource name given relative to that,
     * including packages.
     *
     * @param resource the resource to find
     * @return the URL of the resource, null if none found
     */
    public static URL getResourceURL(String resource) {
        return ClassLoader.getSystemResource(resource);
    }

    /**
     * Returns the path of the given resource. The resource needs to be located
     * inside a source folder and the resource name given relative to that,
     * including packages.
     *
     * @param resource the resource to find
     * @return the path of the resource, null if none found
     */
    public static String getResourcePath(String resource) {
        return getResourceURL(resource).getPath();
    }

    /**
     * Lists the directory contents for a resource folder non-recursively. Only
     * actual file contents are listed, while subdirectories are ignored. Supports
     * access on file-system- and JAR-internal URI locations.A
     * 
     * The format of returned URIs is illustrated by the folloiwing examples:
     * File on disk: "file:/Users/schieder/Code/touchram/ca.mcgill.sel.ram/bin/models/languages/UseCases.core"
     * File from within JAR:
     *    "jar:file:/Users/schieder/Code/touchram/ca.mcgill.sel.ram.gui/releases/20200922_8.1.0_Fall/TouchCORE.jar!/models/languages/UseCases.core"
     *
     * @author Greg Briggs, initial version this is based on, see:
     *         http://stackoverflow.com/a/6247181 A minimum amount of readability
     *         and code-style later added by Max.
     * @param path the path of the resource
     * @return the list of items with their full path, null if path not found
     */
    public static List<String> getResourceListing(String path) {
        URL jarURL = ResourceUtil.class.getProtectionDomain().getCodeSource().getLocation();

        if (jarURL != null) {
            try {
                // If not from a JAR, we search the file-system.
                if (!isRunningFromJar()) {

                    URL dirURL = getResourceURL(path);

                    String[] files = new File(dirURL.toURI()).list();

                    // Create absolute paths.
                    for (int i = 0; i < files.length; i++) {
                        files[i] = new URL(dirURL, files[i]).toExternalForm();
                    }
                    return new ArrayList<String>(Arrays.asList(files));  
                } else {
                    // Alternative is that this TouchCORE instance was launched from a JAR.
                    // In that case we need to generate the combined URI of:
                    // - location of the jar
                    // - relative path within jar and the contained matching files.
                   
                    // Retrieve ALL entries within jar at specified inner location (not just the
                    // matching files.)
                    JarFile jar = new JarFile(URLDecoder.decode(jarURL.getPath(), "UTF-8"));
                    Enumeration<JarEntry> entries = jar.entries();

                    // Prepare empty set that will eventually hold the location of files with
                    // matching file ending
                    Set<String> extractedMatchingResourceFiles = new HashSet<String>();

                    // Iterate through items at specified location in jar.
                    // Add every matching item to the result list (extractedMatchingResourceFiles)
                    while (entries.hasMoreElements()) {
                        String name = entries.nextElement().getName();

                        // Only consider resources that themselves are NO directories.
                        if (name.startsWith(path) && !name.equals(path)) {

                            // Strip name of found resources to only the resource's name, not the full
                            // qualified path / inner path.
                            String entry = name.substring(path.length());

                            // Next we build an actual working URI, including the jar prefix and a "!" to
                            // indicate a relative location within the referred jar.
                            String absolutePath = "jar:" + jarURL.toString() + "!/" +  path + entry;
                            extractedMatchingResourceFiles.add(absolutePath);
                        }
                    }
                    jar.close();

                    return new ArrayList<String>(extractedMatchingResourceFiles);
                }
            } catch (URISyntaxException e) {
                // Shall not happen.
                e.printStackTrace();
            } catch (IOException e) {
                // Shall not happen.
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * Lists the directory contents for a resource folder non-recursively. Filters
     * out all files that don't have the given file extension. This supports both
     * directories in the file system and resources in JARs.
     *
     * @param path          the path of the resource
     * @param fileExtension the file extension to filter for
     * @return the list of items with their full path, null if path not found
     * @see #getResourceListing(String)
     */
    public static List<String> getResourceListing(String path, String fileExtension) {
        List<String> resources = getResourceListing(path);

        if (resources != null) {
            Iterator<String> iterator = resources.iterator();

            while (iterator.hasNext()) {
                String next = iterator.next();

                if (!next.endsWith(fileExtension)) {
                    iterator.remove();
                }
            }
        }

        return resources;
    }

    /**
     * Helper method to determine if the currently running program instance is
     * running from an IDE or a self-contained jar.
     * 
     * @return boolean that is true if TouchCORE was launched from a JAR and false
     *         otherwise.
     * @author Maximilian Schiedermeier
     */
    public static boolean isRunningFromJar() {
        try {
            java.net.URI uri = ResourceUtil.class.getProtectionDomain().getCodeSource().getLocation().toURI();

            boolean isJar = uri.getScheme().equals("jar")
                    || uri.getPath().endsWith("jar");

            return isJar;
        } catch (URISyntaxException e) {
            throw new RuntimeException(
                    "Unable to determine whether this program instance was launched from a JAR or IDE.");
        }
    }

}

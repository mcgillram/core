/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREPerspectiveAction;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Perspective Action</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class COREPerspectiveActionTest extends COREActionTest {

	/**
	 * Constructs a new CORE Perspective Action test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREPerspectiveActionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Perspective Action test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected COREPerspectiveAction getFixture() {
		return (COREPerspectiveAction)fixture;
	}

} //COREPerspectiveActionTest

/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.CoreFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Language Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CORELanguageElementMappingTest extends TestCase {

	/**
	 * The fixture for this CORE Language Element Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CORELanguageElementMapping fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CORELanguageElementMappingTest.class);
	}

	/**
	 * Constructs a new CORE Language Element Mapping test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CORELanguageElementMappingTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this CORE Language Element Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CORELanguageElementMapping fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this CORE Language Element Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CORELanguageElementMapping getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCORELanguageElementMapping());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CORELanguageElementMappingTest

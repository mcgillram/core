/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREModelElementMapping;
import ca.mcgill.sel.core.CoreFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Model Element Mapping</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class COREModelElementMappingTest extends TestCase {

	/**
	 * The fixture for this CORE Model Element Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREModelElementMapping fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(COREModelElementMappingTest.class);
	}

	/**
	 * Constructs a new CORE Model Element Mapping test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREModelElementMappingTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this CORE Model Element Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(COREModelElementMapping fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this CORE Model Element Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREModelElementMapping getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCOREModelElementMapping());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //COREModelElementMappingTest

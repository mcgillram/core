/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.InterLanguageMappingEnd;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Inter Language Mapping End</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link ca.mcgill.sel.core.InterLanguageMappingEnd#getName() <em>Name</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class InterLanguageMappingEndTest extends TestCase {

	/**
	 * The fixture for this Inter Language Mapping End test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterLanguageMappingEnd fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(InterLanguageMappingEndTest.class);
	}

	/**
	 * Constructs a new Inter Language Mapping End test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterLanguageMappingEndTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Inter Language Mapping End test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(InterLanguageMappingEnd fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Inter Language Mapping End test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterLanguageMappingEnd getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createInterLanguageMappingEnd());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#getName() <em>Name</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.core.InterLanguageMappingEnd#getName()
	 * @generated
	 */
	public void testGetName() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link ca.mcgill.sel.core.InterLanguageMappingEnd#setName(java.lang.String) <em>Name</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ca.mcgill.sel.core.InterLanguageMappingEnd#setName(java.lang.String)
	 * @generated
	 */
	public void testSetName() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //InterLanguageMappingEndTest

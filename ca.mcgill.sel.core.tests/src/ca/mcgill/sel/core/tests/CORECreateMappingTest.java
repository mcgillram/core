/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CORECreateMapping;
import ca.mcgill.sel.core.CoreFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Create Mapping</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CORECreateMappingTest extends COREPerspectiveActionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CORECreateMappingTest.class);
	}

	/**
	 * Constructs a new CORE Create Mapping test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CORECreateMappingTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Create Mapping test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CORECreateMapping getFixture() {
		return (CORECreateMapping)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCORECreateMapping());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CORECreateMappingTest

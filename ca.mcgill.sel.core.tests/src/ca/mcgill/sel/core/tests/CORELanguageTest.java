/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.CORELanguage;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Language</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CORELanguageTest extends COREArtefactTest {

	/**
	 * Constructs a new CORE Language test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CORELanguageTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Language test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CORELanguage getFixture() {
		return (CORELanguage)fixture;
	}

} //CORELanguageTest

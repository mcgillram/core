/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREMappingCardinality;
import ca.mcgill.sel.core.CoreFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Mapping Cardinality</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class COREMappingCardinalityTest extends TestCase {

	/**
	 * The fixture for this CORE Mapping Cardinality test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREMappingCardinality fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(COREMappingCardinalityTest.class);
	}

	/**
	 * Constructs a new CORE Mapping Cardinality test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREMappingCardinalityTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this CORE Mapping Cardinality test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(COREMappingCardinality fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this CORE Mapping Cardinality test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREMappingCardinality getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCOREMappingCardinality());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //COREMappingCardinalityTest

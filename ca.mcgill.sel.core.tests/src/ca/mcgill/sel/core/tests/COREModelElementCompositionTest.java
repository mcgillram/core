/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREModelElementComposition;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Model Element Composition</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class COREModelElementCompositionTest extends TestCase {

	/**
	 * The fixture for this CORE Model Element Composition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREModelElementComposition<?> fixture = null;

	/**
	 * Constructs a new CORE Model Element Composition test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREModelElementCompositionTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this CORE Model Element Composition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(COREModelElementComposition<?> fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this CORE Model Element Composition test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COREModelElementComposition<?> getFixture() {
		return fixture;
	}

} //COREModelElementCompositionTest

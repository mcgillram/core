/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.COREFeatureImpactNode;
import ca.mcgill.sel.core.CoreFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>CORE Feature Impact Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class COREFeatureImpactNodeTest extends COREImpactNodeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(COREFeatureImpactNodeTest.class);
	}

	/**
	 * Constructs a new CORE Feature Impact Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public COREFeatureImpactNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this CORE Feature Impact Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected COREFeatureImpactNode getFixture() {
		return (COREFeatureImpactNode)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createCOREFeatureImpactNode());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //COREFeatureImpactNodeTest

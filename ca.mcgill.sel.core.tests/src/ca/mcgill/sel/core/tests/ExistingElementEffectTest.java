/**
 */
package ca.mcgill.sel.core.tests;

import ca.mcgill.sel.core.ExistingElementEffect;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Existing Element Effect</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ExistingElementEffectTest extends ActionEffectTest {

	/**
	 * Constructs a new Existing Element Effect test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExistingElementEffectTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Existing Element Effect test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ExistingElementEffect getFixture() {
		return (ExistingElementEffect)fixture;
	}

} //ExistingElementEffectTest

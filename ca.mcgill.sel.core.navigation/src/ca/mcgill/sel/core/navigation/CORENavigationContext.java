package ca.mcgill.sel.core.navigation;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREPerspective;
import ca.mcgill.sel.core.COREScene;
import ca.mcgill.sel.core.impl.COREConcernImpl;
import ca.mcgill.sel.core.util.COREArtefactUtil;


/**
 * CORENavigationContext is a helper class that keeps track of the current modelling context of the modeller.
 * As such, it keeps track of the current concern, the current perspective, the current feature and the current
 * artefact/model being displayed. It also keeps track of the other model being displayed in the split view,
 * if it is being used.
 * 
 * @author joerg
 */
public class CORENavigationContext {
	
	// the current concern
	private static COREConcern currentConcern;
	
	// the current perspective
	private static COREPerspective currentPerspective;
	
	// the current scene
	private static COREScene currentScene;

	// the current artefact
	private static COREArtefact currentArtefact;
	
	// the current model
	private static EObject currentModel;
	
	/**
	 * Getter to query the current concern.
	 * 
	 * @return the current concern
	 */
	public static COREConcern getCurrentConcern() {
		return currentConcern;
	}

	/**
	 * Setter to set the current concern.
	 * 
	 * @param concern the reference to the concern
	 */
	public static void setCurrentConcern(COREConcern concern) {
		currentConcern = concern;
	}

	/**
	 * Getter to query the current perspective.
	 * 
	 * @return the current perspective
	 */
	public static COREPerspective getCurrentPerspective() {
		return currentPerspective;
	}

	/**
	 * Setter to set the current perspective.
	 * 
	 * @param currentPerspective the reference to the perspective to be used
	 */
	public static void setCurrentPerspective(COREPerspective perspective) {
		currentPerspective = perspective;
	}

	/**
	 * Getter to query the current scene.
	 * 
	 * @return the current scene
	 */
	public static COREScene getCurrentScene() {
		return currentScene;
	}

	/**
	 * Setter to set the current scene.
	 * 
	 * @param scene the reference to the scene to be used
	 */
	public static void setCurrentScene(COREScene scene) {
		currentScene = scene;
	}

	/**
	 * Getter to query the current artefact.
	 * 
	 * @return the current artefact
	 */
	public static COREArtefact getCurrentArtefact() {
		return currentArtefact;
	}

	/**
	 * Setter to set the current artefact.
	 * This method also sets the current model, as the current artefact should always be consistent with
	 * the current model.
	 * 
	 * @param currentArtefact the reference to the artefact to be used
	 */
	public static void setCurrentArtefact(COREArtefact artefact) {
		currentArtefact = artefact;
		if (currentArtefact instanceof COREExternalArtefact) {
			currentModel = ((COREExternalArtefact) currentArtefact).getRootModelElement();
		}
	}

	/**
	 * Getter to query the current model.
	 * 
	 * @return the current model
	 */
	public static EObject getCurrentModel() {
		return currentModel;
	}

	/**
	 * Setter to set the current model.
	 * This method also sets the current artefact, as the current artefact should always be consistent with
	 * the current model.
	 * 
	 * @param currentModel the reference to the model to be used
	 */
	public static void setCurrentModel(EObject model) {
		currentModel = model;
        if (!(model instanceof COREConcernImpl)) {
            currentArtefact = COREArtefactUtil.getReferencingExternalArtefact(currentModel);
        } else {
            currentArtefact = null;
        }
	}

	
	
}

/**
 */
package ca.mcgill.sel.core.provider;


import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link ca.mcgill.sel.core.COREArtefact} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class COREArtefactItemProvider extends CORENamedElementItemProvider {
    /**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    public COREArtefactItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

    /**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTemporaryConcernPropertyDescriptor(object);
			addScenePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

    /**
	 * This adds a property descriptor for the Temporary Concern feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addTemporaryConcernPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_COREArtefact_temporaryConcern_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_COREArtefact_temporaryConcern_feature", "_UI_COREArtefact_type"),
				 CorePackage.Literals.CORE_ARTEFACT__TEMPORARY_CONCERN,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This adds a property descriptor for the Scene feature.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    protected void addScenePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_COREArtefact_scene_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_COREArtefact_scene_feature", "_UI_COREArtefact_type"),
				 CorePackage.Literals.CORE_ARTEFACT__SCENE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

    /**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES);
			childrenFeatures.add(CorePackage.Literals.CORE_ARTEFACT__MODEL_EXTENSIONS);
			childrenFeatures.add(CorePackage.Literals.CORE_ARTEFACT__UI_ELEMENTS);
			childrenFeatures.add(CorePackage.Literals.CORE_ARTEFACT__CI_ELEMENTS);
		}
		return childrenFeatures;
	}

    /**
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

    /**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public String getText(Object object) {
		String label = ((COREArtefact)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_COREArtefact_type") :
			getString("_UI_COREArtefact_type") + " " + label;
	}
    

    /**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(COREArtefact.class)) {
			case CorePackage.CORE_ARTEFACT__MODEL_REUSES:
			case CorePackage.CORE_ARTEFACT__MODEL_EXTENSIONS:
			case CorePackage.CORE_ARTEFACT__UI_ELEMENTS:
			case CorePackage.CORE_ARTEFACT__CI_ELEMENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

    /**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
	 * @generated
	 */
    @Override
    protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_ARTEFACT__MODEL_REUSES,
				 CoreFactory.eINSTANCE.createCOREModelReuse()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_ARTEFACT__MODEL_EXTENSIONS,
				 CoreFactory.eINSTANCE.createCOREModelExtension()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_ARTEFACT__UI_ELEMENTS,
				 CoreFactory.eINSTANCE.createCOREUIElement()));

		newChildDescriptors.add
			(createChildParameter
				(CorePackage.Literals.CORE_ARTEFACT__CI_ELEMENTS,
				 CoreFactory.eINSTANCE.createCORECIElement()));
	}

}

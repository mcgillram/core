package ca.mcgill.sel.core.perspective;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.CORELanguageElementMapping;
import ca.mcgill.sel.core.Cardinality;
import ca.mcgill.sel.core.MappingEnd;

/**
 * Manages template types
 * @author hyacinthali
 *
 */
public class TemplateType {
    
    /**
     * Singleton instance variable.
     */
    public static TemplateType INSTANCE = new TemplateType();
    
    /**
     * Singleton pattern - initialize only a single instance.
     */
    private TemplateType() {

    }  
    

    public ActionType getCreateType(CORELanguageElementMapping mappingType, String currentRole) {

        ActionType actionType = null;

        
        MappingEnd fromMappingEnd = mappingType.getMappingEnds().get(0);
        MappingEnd toMappingEnd = mappingType.getMappingEnds().get(1);
        boolean isFrom = fromMappingEnd.getRoleName().equals(currentRole);

        if (
        // C1.1 condition
        (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY
                && toMappingEnd.getCardinality() == Cardinality.OPTIONAL && isFrom)
                // C11.2 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY && !isFrom)) {
            actionType = ActionType.CAN_CREATE;
        } else if (
        // C2.1 and C2.1 condition
        fromMappingEnd.getCardinality() == Cardinality.COMPULSORY
                && toMappingEnd.getCardinality() == Cardinality.COMPULSORY) {
            actionType = ActionType.CREATE;
        } else if (
        // c3.1 condition
        (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY
                && toMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE && isFrom)
                // C12.2 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY && !isFrom)) {
            actionType = ActionType.CAN_CREATE_MANY;
        } else if (
        // C4.1 condition
        (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY
                && toMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE && isFrom)
                // C13.2 condition
                || (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY && !isFrom)) {
            actionType = ActionType.CREATE_AT_LEAST_ONE;
        } else if (
        // C5.1 and C5.2 condition
        fromMappingEnd.getCardinality() == Cardinality.OPTIONAL
                && toMappingEnd.getCardinality() == Cardinality.OPTIONAL) {
            actionType = ActionType.CAN_CREATE_OR_USE_NON_MAPPED;
        } else if (
        // C6.1 condition
        (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE
                && toMappingEnd.getCardinality() == Cardinality.OPTIONAL && isFrom)
                // C7.1 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE
                        && toMappingEnd.getCardinality() == Cardinality.OPTIONAL && isFrom)
                // C14.2 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE && !isFrom)
                // C15.2 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL
                        && toMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE && !isFrom)) {
            actionType = ActionType.CAN_CREATE_OR_USE;
        } else if (
        // C8.1 and C8.2 condition
        (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE
                && toMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE)
                // C10.1 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE && isFrom)
                // C16.2 condition
                || (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE
                        && toMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE && !isFrom)) {
            actionType = ActionType.CREATE_OR_USE_AT_LEAST_ONE;
        } else if (
        // C9.1 and C9.2 condition
        (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE
                && toMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE)
                // C10.2 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE && !isFrom)
                // C16.1 condition
                || (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE
                        && toMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE && isFrom)) {
            actionType = ActionType.CAN_CREATE_OR_USE_MANY;
        } else if (
        // C1.2 condition
        (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY
                && toMappingEnd.getCardinality() == Cardinality.OPTIONAL && !isFrom)
                // C11.1 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY && isFrom)) {
            actionType = ActionType.CREATE_OR_USE_NON_MAPPED;
        } else if (
        // C3.2 condition
        (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY
                && toMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE && !isFrom)
                // C4.2 condition
                || (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE && !isFrom)
                // C12.1 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY && isFrom)
                // C13.1 condition
                || (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY && isFrom)) {
            actionType = ActionType.CREATE_OR_USE;
        } else if (
        // C6.2 condition
        (fromMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE
                && toMappingEnd.getCardinality() == Cardinality.OPTIONAL & !isFrom)
                // C14.1 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL
                        && toMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE & isFrom)) {
            actionType = ActionType.CREATE_OR_USE_NON_MAPPED_AT_LEAST_ONE;
        } else if (
        // C7.2 condition
        (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE
                && toMappingEnd.getCardinality() == Cardinality.OPTIONAL && !isFrom)
                // C15.1 condition
                || (fromMappingEnd.getCardinality() == Cardinality.OPTIONAL
                        && toMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE && isFrom)) {
            actionType = ActionType.CAN_CREATE_OR_USE_NON_MAPPED_MANY;
        }

        return actionType;
    }

    /**
     * Gets the delete type. given the mapping end of the model element to be deleted.
     * @param mappingEnd - mapping end of the element to be deleted.
     * @return the action delete type.
     */
    public ActionType getDeleteType(MappingEnd mappingEnd) {
        ActionType deleteType = null;
        if (mappingEnd.getCardinality() == Cardinality.COMPULSORY) {
            deleteType = ActionType.DELETE_OTHERS;
        } else if (mappingEnd.getCardinality() == Cardinality.OPTIONAL
                || mappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE) {
            deleteType = ActionType.JUST_DELETE;
        } else if (mappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE) {
            deleteType = ActionType.DELETE_SINGLEMAPPED;
        }
        return deleteType;
    }
    
    /**
     * Get mapping details {@link MappingDetail} for a binary mapping.
     * @param elements - maps between role name and the corresponding model element.
     * @param mappingType 
     * @return the mapping mappingType
     */
    public MappingDetail getMappingDetails(Map<String, EObject> elements, CORELanguageElementMapping type) {
        MappingDetail mappingDetail = new MappingDetail();
        mappingDetail.setMap(false);
        
        if (type.getMappingEnds().size() != 2) {
            mappingDetail.setMessage("The create mapping action only supports binary mapping");
            return mappingDetail;
        }
       
        mappingDetail.setMap(true);
        MappingEnd firstMappingEnd = type.getMappingEnds().get(0);
        MappingEnd secondMappingEnd = type.getMappingEnds().get(1);
        
        // 1. Compulsory Optional (first 1 -- 0..1 second) Template
        if (firstMappingEnd.getCardinality() == Cardinality.COMPULSORY 
                && secondMappingEnd.getCardinality() == Cardinality.OPTIONAL) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, MappingType.COMPULSORY_OPTIONAL, 
                    false);
//            mappingType.setFirstElement(elements.get(firstMappingEnd.getRoleName()));
//            mappingType.setSecondElement(elements.get(secondMappingEnd.getRoleName()));
//            mappingType.setType(MappingType.COMPULSORY_OPTIONAL);
            
        } 
        // this is inverse condition of 1 (11)
        else if (secondMappingEnd.getCardinality() == Cardinality.COMPULSORY 
                && firstMappingEnd.getCardinality() == Cardinality.OPTIONAL) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, MappingType.COMPULSORY_OPTIONAL, 
                    true);
//            mappingType.setFirstElement(elements.get(secondMappingEnd.getRoleName()));
//            mappingType.setSecondElement(elements.get(firstMappingEnd.getRoleName()));
//            mappingType.setType(MappingType.COMPULSORY_OPTIONAL);
        } 
        
        // 2. Compulsory Compulsory (from 1 -- 1 to) Template
        else if (firstMappingEnd.getCardinality() == Cardinality.COMPULSORY 
                && secondMappingEnd.getCardinality() == Cardinality.COMPULSORY) {
            // The mapping has already been created when the elements were created.
            mappingDetail.setMessage("The mapping has already been created when the elements were created.");
            mappingDetail.setMap(false);
            
        }
        
        // 3. Compulsory Optional-Multiple (from 1 -- 0..* to) Template
        else if (firstMappingEnd.getCardinality() == Cardinality.COMPULSORY 
                && secondMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, 
                    MappingType.COMPULSORY_OPTIONAL_MULTIPLE, false);
            
        }
        // this is inverse condition of 3 (12)
        else if (secondMappingEnd.getCardinality() == Cardinality.COMPULSORY 
                && firstMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, 
                    MappingType.COMPULSORY_OPTIONAL_MULTIPLE, true);
        }
        
        // 4. Compulsory Compulsory-Multiple (from 1 -- 1..* to) Template
        else if (firstMappingEnd.getCardinality() == Cardinality.COMPULSORY 
                && secondMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, 
                    MappingType.COMPULSORY_COMPULSORY_MULTIPLE, false);
            
        }
        // this is inverse condition of 4 (13)
        else if (secondMappingEnd.getCardinality() == Cardinality.COMPULSORY 
                && firstMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, 
                    MappingType.COMPULSORY_COMPULSORY_MULTIPLE, true);
            
        }
        
        // 5. Optional Optional (from 0..1 -- 0..1 to) Template
        else if (firstMappingEnd.getCardinality() == Cardinality.OPTIONAL 
                && secondMappingEnd.getCardinality() == Cardinality.OPTIONAL) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, 
                    MappingType.OPTIONAL_OPTIONAL, false);
            
        }
        
        // 6. Compulsory-Multiple Optional (from 1..* -- 0..1 to) Template
        else if (firstMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE 
                && secondMappingEnd.getCardinality() == Cardinality.OPTIONAL) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, 
                    MappingType.COMPULSORY_MULTIPLE_OPTIONAL, false);
            
        }
        // this is inverse of 6 (14)
        else if (secondMappingEnd.getCardinality() == Cardinality.COMPULSORY_MULTIPLE 
                && firstMappingEnd.getCardinality() == Cardinality.OPTIONAL) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, 
                    MappingType.COMPULSORY_MULTIPLE_OPTIONAL, true);
            
        }
        
        // 7. Optional-Multiple Optional (from 0..* -- 0..1 to) Template
        else if (firstMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE 
                && secondMappingEnd.getCardinality() == Cardinality.OPTIONAL) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, 
                    MappingType.OPTIONAL_MULTIPLE_OPTIONAL, true);
        }
        // this is inverse of 7 (15)
        else if (secondMappingEnd.getCardinality() == Cardinality.OPTIONAL_MULTIPLE 
                && firstMappingEnd.getCardinality() == Cardinality.OPTIONAL) {
            setMappingElements(mappingDetail, elements, firstMappingEnd, secondMappingEnd, 
                    MappingType.OPTIONAL_MULTIPLE_OPTIONAL, false);
        } else {
            
        }
        
        return mappingDetail;
    }
    
    /**
     * Set the mapping details of a user initiated mapping.
     * @param mappingDetail - the mapping details which needs to be updated
     * @param elements - a map of role names and their corresponding model elements to be mapped.
     * @param firstMappingEnd
     * @param secondMappingEnd
     * @param mappingType
     * @param inverse - a flag which indicated whether the mapping template is an inverse or not. 
     * E.g., the template (1 -- 0.1) is an inverse of (0.1 -- 1).
     * 
     * @author Hyacinth Ali
     */
    private void setMappingElements(MappingDetail mappingDetail, Map<String, EObject> elements, MappingEnd firstMappingEnd, 
            MappingEnd secondMappingEnd, MappingType mappingType, boolean inverse) {
        
        mappingDetail.setType(mappingType);
        
        if (inverse) {
            mappingDetail.setFirstElement(elements.get(secondMappingEnd.getRoleName()));
            mappingDetail.setSecondElement(elements.get(firstMappingEnd.getRoleName()));
        } else {
            mappingDetail.setFirstElement(elements.get(firstMappingEnd.getRoleName()));
            mappingDetail.setSecondElement(elements.get(secondMappingEnd.getRoleName()));
  
        }
        
    }

}

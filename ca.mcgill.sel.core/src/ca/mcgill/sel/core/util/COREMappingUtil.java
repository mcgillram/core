package ca.mcgill.sel.core.util;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREArtefact;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElementComposition;

/**
 * Helper class with convenient static methods for working with CORE Mappings.
 *
 * @author joerg
 */
public final class COREMappingUtil {
    
    /**
     * Creates a new instance.
     */
    private COREMappingUtil() {
        
    }

    /**
     * Method that checks whether or not a mapping (or the mapping it contains) maps to the model
     * element me.
     * 
     * @param cm the mapping
     * @param me the model element under consideration
     * @return whether or not cm or contained mappings map to me
     */
    public static boolean isElementMappedTo(COREMapping<?> cm, EObject me) {
        if (cm.getTo() == me) {
            return true;
        }
        for (COREMapping<?> containedMapping : cm.getMappings()) {
            if (isElementMappedTo(containedMapping, me)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Collects all model elements that are mapped to modelElement in the current realization model as well as any
     * extended or reused realization models, and the models they extend or reuse, etc...
     *
     * @param modelElement the model element for which to collect all mapped elements
     * @return the set of model elements that are mapped to modelElement. This set includes modelElement as well.
     */
    public static Set<EObject> findMappedElements(EObject modelElement) {
        Set<EObject> result = new HashSet<EObject>();
        result.add(modelElement);
        
        COREArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(modelElement);

        Set<COREModelComposition> modelCompositions = new HashSet<COREModelComposition>();
        modelCompositions.addAll(artefact.getModelExtensions());
        modelCompositions.addAll(artefact.getModelReuses());

        for (COREModelComposition modelComposition : modelCompositions) {
            for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
                COREMapping<?> mapping = (COREMapping<?>) composition;
                if (mapping.getTo() == modelElement) {
                    result.addAll(findMappedElements((EObject) mapping.getFrom()));
                }
            }
        }
        return result;
    }


}

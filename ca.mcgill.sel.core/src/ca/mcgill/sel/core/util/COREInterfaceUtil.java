package ca.mcgill.sel.core.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREExternalArtefact;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREUIElement;

/**
 * Helper class with convenient static methods for working with CORE Interfaces.
 *
 * @author mschoettle
 * @author joerg
 */
public final class COREInterfaceUtil {

    /**
     * Creates a new instance of {@link COREInteraceUtil}.
     */
    private COREInterfaceUtil() {
        // suppress default constructor
    }

    /**
     * Determines whether or not "object" is part of the usage interface of the concern.
     * In the case where the caller already has a reference to the COREExternalArtefact that refers to the
     * model that contains object, then performance-wise it is better to call the overloaded isPublic method
     * that has an additional COREModelArtefact parameter.
     * 
     * @param object the object that we want to know about
     * @return whether or not the object is part of the usage interface of the concern
     */
    public static boolean isPublic(EObject object) {
        COREExternalArtefact artefact = COREArtefactUtil.getReferencingExternalArtefact(object);
        for (COREUIElement uiElement : artefact.getUiElements()) {
            if (uiElement.getModelElement() == object) {
                return true;
            }
        }
        return false;
    }
   
    /**
     * Determines whether or not "object" is part of the usage interface of the given artefact.
     * Performance-wise this isPublic method if significantly faster than the one that doesn't have the
     * COREExternalArtefact parameter.
     * 
     * @param artefact the artefact that refers to the model that contains object
     * @param object the object that we want to know about
     * @return whether or not the object is part of the usage interface of the concern
     */
    public static boolean isPublic(COREExternalArtefact artefact, EObject object) {
        for (COREUIElement uiElement : artefact.getUiElements()) {
            if (uiElement.getModelElement() == object) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Returns the list of all reuses in a concern. 
     * Reuses that are extending other reuses are 
     * @param concern we are in
     * @return the list of reuses in the reuses
     */
    public static List<COREReuse> getReuses(COREConcern concern) {
        List<COREReuse> reuses = new ArrayList<COREReuse>();
        
        for (COREReuse coreReuse : concern.getReuses()) {
            reuses.add(coreReuse);
        }
        
        return reuses;
    }
}

/**
 */
package ca.mcgill.sel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delete Effect</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ca.mcgill.sel.core.CorePackage#getDeleteEffect()
 * @model
 * @generated
 */
public interface DeleteEffect extends ExistingElementEffect {
} // DeleteEffect
